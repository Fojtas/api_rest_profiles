from django.urls import path , include
from rest_framework.routers import DefaultRouter
from profiles_API import views

router= DefaultRouter()
router.register('viewSet', views.ViewSet ,basename='vieSet')
router.register('profiles', views.UserProfileViewSet ,basename='UserProfileViewSet')
router.register('feeds', views.ProfileFeedItemViewSet, basename='ProfileFeedItemViewSet')

urlpatterns = [
    path('hello/', views.HelloApiView.as_view()),
    path('login/', views.LoginApiView.as_view()),
    path('', include(router.urls))

]
