from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework import filters
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework.permissions import IsAuthenticated


from profiles_API import serializers
from profiles_API import models
from profiles_API import permissions




class HelloApiView(APIView):
    """test API view"""

    serializer_class = serializers.HelloSerializer

    def get(self, request, format = None):
        """return a list of APIview features"""
        an_apiview = [
        'uses HTTP methods as functions (get,post,parch,put,delete)',
        'is similiar to traditional Django View',
        '3',
        '4'
        ]

        return Response({'message': 'Hello', 'an_apiview': an_apiview})


    def post(self,request):
        """Create a hello message with our name"""

        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            name = serializer.validated_data.get('name')
            message = f"Hello {name}"
            return Response({'message': message})
        else:
            return Response(
                serializer.errors,
                status= status.HTTP_400_BAD_REQUEST
            )


    def put(self,request,pk=None):
        return Response({"method": "PUT"})


    def patch(self,requst,pk=None):
        return Response({"method": "PATCH"})


    def delete(self,request,pk=None):
        return Response({"method": "DELETE"})




class ViewSet(viewsets.ViewSet):
    """Test View Set"""

    def list(self,request):

        a_viewset = [
        '1',
        '2',
        '3'
        ]

        return Response({"karel": a_viewset})


class UserProfileViewSet(viewsets.ModelViewSet):
    """updating and creating profiles"""
    serializer_class = serializers.UserProfileSerializer
    queryset = models.UserProfile.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.UpdateOwnProfile,)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name', 'email',)


class LoginApiView(ObtainAuthToken):
    """create token for new user"""
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class ProfileFeedItemViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    serializer_class = serializers.ProfileFeedItemSerializer
    queryset = models.ProfileFeedItem.objects.all()
    permission_classes = (
    permissions.UpdateOwnStatus,
    IsAuthenticated)

    def perform_create(self, serializer):
            serializer.save(user_profile = self.request.user)
